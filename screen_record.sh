#!/bin/sh

# PUT IT TO ~/.local/bin AND chmod +x IT

# NEEDS wf-recorder (and librav1e ig, if you dont have one), slurp and libnotify

case $1 in
    "record")
        notify-send "Starting record..."
        wf-recorder -f "$(xdg-user-dir VIDEOS)/record_$(date +'%s.mkv')" -c librav1e
    ;;
    "record-geom")
        geom="$(slurp)"
        notify-send "Starting record..."
        wf-recorder -g "$geom" -f "$(xdg-user-dir VIDEOS)/record_$(date +'%s.webm')" -c librav1e
    ;;
    "stop")
        notify-send "Stopping record..."
        killall -s SIGINT wf-recorder
    ;;
esac