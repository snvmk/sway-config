# sway-config

heres my ~/.config/sway/config (a monolith config yeah)

# needs

- bemenu (and wayland front-end of course)
- kitty (or you can change the $term to something else; i need it just for ligatures)
- j4-dmenu-desktop (for app menu)
- swaylock
- change your wallpaper of course (it is set to ~/Pictures/wallpapers/san_francisco.jpg)
- swayidle
- grim, slurp, swappy
- wl-color-picker
- clipman
- wl-clipboard
- screen_record.sh script
- exit_dialog.sh
- pulseaudio
- brightnessctl
- Fira Code (Nerd Font)
- systemd
- gtk
