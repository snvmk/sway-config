#!/bin/sh

# PUT THIS FILE TO $HOME/.local/bin AND chmod +x IT

_lock=" Lock"
_exit=" Exit"
_poweroff=" Poweroff"
_reboot="ﰇ Reboot"
_hibernate=" Hibernate"
_suspend="⏾ Suspend"

option=$(printf "$_lock\n$_exit\n$_poweroff\n$_reboot\n$_hibernate\n$_suspend" | bemenu -p "system")

case $option in
	$_lock) swaylock -f -c 000000 ;;
	$_exit) swaymsg exit ;;
	$_poweroff) systemctl shutdown ;;
	$_reboot) systemctl reboot ;;
	$_hibernate) systemctl hibernate && swaylock -f -c 000000 ;;
	$_suspend) systemctl suspend && swaylock -f -c 000000 ;;
esac